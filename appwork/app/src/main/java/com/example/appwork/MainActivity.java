package com.example.appwork;


import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.analyzer.HorizontalWidgetRun;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;

import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;




public class MainActivity extends AppCompatActivity {



    ProgressBar progressBar;
    TextView textView;
    Dialog dialog;
    boolean error = false;
    int i = 0;
    boolean downloadok = false;
    boolean act = true;



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        firstdialog();
    }
    public void firstdialog(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.first_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
    public void errordialog(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.error_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
    public void dlfail(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.downloadfail);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
    public void dlcomplete(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlcomplete);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void dialogclose(View view) {
        dialog.dismiss();
        determine();
    }

    public void dialogend(View view) {
        dialog.dismiss();
        finish();
    }
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            i++;
            Log.d("AAA","i ==" + i);
            if (downloadok == true && i < 172800){
                dlcomplete();
            } else if (downloadok == false && i > 172800 ){
                dlfail();
                //sent error message
            } else {
                handler.postDelayed(this, 1000);
            }
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static int storage(Context context){
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = stat.getAvailableBytes();
        long megAvailable = bytesAvailable / 1048576;
        String myString = Long.toString(megAvailable);
        int myint = Integer.valueOf(myString);
        Log.d("AAA",String.valueOf(myint));
        return myint;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void determine() {
        if (error) {
            errordialog();
        } else {
            Log.d("AAA", "twice");
            switch (checkNetworkStatus(this)){
                case "wifi" :
                case "mobileData":
                    Log.d("AAA","enter mobileData");
                    if(storage(this) > 3072){
                        handler.postDelayed(runnable,1000);
                        download();
                    } else{
                        Intent StorageSettingsIntent = new Intent("android.settings.INTERNAL_STORAGE_SETTINGS");
                        startActivity(StorageSettingsIntent);
                        finish();
                    }
                    break;
                case "noNetwork":
                    Intent wifiSettingsIntent = new Intent("android.settings.WIFI_SETTINGS");
                    startActivity(wifiSettingsIntent);
                    finish();
                    break;
                default:
                    finish();
            }
        }
    }
    public static String checkNetworkStatus(Context context) {


        String networkStatus = " ";
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if( wifi.isConnected() ) {
            networkStatus = "wifi";
        } else if( mobile.isConnected() ) {
            networkStatus = "mobileData";
        } else {
            networkStatus = "noNetwork";
        }
        Log.d("AAA", networkStatus);

        return networkStatus;

    }

    public void download (){

        dialog.setContentView(R.layout.dldialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        progressBar = (ProgressBar) dialog.findViewById(R.id.pb);
        textView = (TextView) dialog.findViewById(R.id.num);

        String getUrl = "http://ftp.ntu.edu.tw/Apache/accumulo/2.0.1/accumulo-2.0.1-bin.tar.gz";

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(getUrl));
        String title = URLUtil.guessFileName(getUrl,null,null);
        request.setTitle(title);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,title);

        DownloadManager downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
//        downloadManager.enqueue(request);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadManager.enqueue(request));

        new Thread(new Runnable() {

            @Override
            public void run() {
                while (act) {
                    Cursor cursor = downloadManager.query(query);
                    cursor.moveToFirst();
                    int downloadedBytes = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int totalBytes = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                    int progress_ps = (int) ((downloadedBytes * 100l) / totalBytes);
                    Log.d("BBB", toString().valueOf(progress_ps));
                    progressBar.setProgress(progress_ps);
                    String prog = toString().valueOf(progress_ps);
                    setText(textView,prog);
                    if(progress_ps == 100) {
                        act = false;
                        downloadok = true;
                        dialog.dismiss();
                    }
                    cursor.close();
                }
            }
            }).start();
    }
    private void setText(TextView text, String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value + "% Complete");
            }
        });
    }
}








